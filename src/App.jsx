import React from "react"
import HeaderUI from "./Components/Header/header"


function App() {

  return (
    <div className="App">
      <HeaderUI />
    </div>
  )
}

export default App
